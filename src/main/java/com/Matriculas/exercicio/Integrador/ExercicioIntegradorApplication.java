package com.Matriculas.exercicio.Integrador;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExercicioIntegradorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExercicioIntegradorApplication.class, args);
	}

}
