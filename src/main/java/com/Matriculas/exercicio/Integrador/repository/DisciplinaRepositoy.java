package com.Matriculas.exercicio.Integrador.repository;

import com.Matriculas.exercicio.Integrador.model.Disciplina;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DisciplinaRepositoy extends JpaRepository<Disciplina, Long> {
}
