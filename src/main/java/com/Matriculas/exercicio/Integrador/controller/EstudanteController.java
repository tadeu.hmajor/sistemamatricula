package com.Matriculas.exercicio.Integrador.controller;

import com.Matriculas.exercicio.Integrador.model.Disciplina;
import com.Matriculas.exercicio.Integrador.model.Estudante;
import com.Matriculas.exercicio.Integrador.service.DisciplinaService;
import com.Matriculas.exercicio.Integrador.service.EstudanteService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
@RestController
@CrossOrigin(origins = "http://localhost:8080/", maxAge = 3600)
@RequestMapping("/estudantes")
public class EstudanteController {
    @Autowired
    private EstudanteService estudanteService;
    @Autowired
    private DisciplinaService disciplinaService;

    @PostMapping
    public ResponseEntity<Estudante> addEstudante(@Valid @RequestBody Estudante estudante){

        Estudante estudanteAdd = estudanteService.add(estudante);
        return  ResponseEntity.status(HttpStatus.CREATED).body(estudanteAdd);
    }

    @GetMapping
    public List<Estudante> listAllEstudantes(){
        return  estudanteService.listEstudante();
    }

    @DeleteMapping("/{id}")
    public  void deleteEstudante(@PathVariable Long id){
        estudanteService.delete(id);
    }

    @PutMapping("/{id}")
    public  ResponseEntity<Estudante> alterEstudante(@Valid @PathVariable Long id, @RequestBody Estudante estudante){
        return  ResponseEntity.ok(estudanteService.alter(id, estudante));
    }

    @GetMapping("/{id}")
    public  Estudante listaEstudanteId(@PathVariable Long id){
        return ResponseEntity.ok(estudanteService.validateEstudante(id)).getBody();
    }

    @GetMapping("/name/{name}")
    public List<Estudante> listName(@PathVariable String name){
        return estudanteService.listEstudanteNome(name);
    }


    @PutMapping("/matriculaEstudante/{idEstudante}/{idDisciplina}")
    public ResponseEntity<Estudante> matricularEstudante(@PathVariable Long idEstudante,@PathVariable Long idDisciplina){

        return ResponseEntity.ok(estudanteService.addDisciplinas(idEstudante,idDisciplina));
    }

}
