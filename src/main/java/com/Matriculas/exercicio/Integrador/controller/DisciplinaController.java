package com.Matriculas.exercicio.Integrador.controller;

import com.Matriculas.exercicio.Integrador.model.Disciplina;
import com.Matriculas.exercicio.Integrador.model.Estudante;
import com.Matriculas.exercicio.Integrador.service.DisciplinaService;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:8080/", maxAge = 3600)
@RequestMapping("/disciplinas")
@Component
public class DisciplinaController {

    @Autowired
    private DisciplinaService disciplinaService;

    @Autowired
    private EstudanteController estudanteController;

    @PostMapping
    public ResponseEntity<Disciplina> addDisciplina(@Valid @RequestBody Disciplina disciplina){

        Disciplina disciplinaAdd = disciplinaService.add(disciplina);
        return  ResponseEntity.status(HttpStatus.CREATED).body(disciplinaAdd);
    }

    @GetMapping
    public List<Disciplina> listAllDisciplinas(){return disciplinaService.listDisciplina();}

    @DeleteMapping("/{codDisciplina}")
    public void deleteDisciplina(@PathVariable Long codDisciplina){
        disciplinaService.delete(codDisciplina);
    }

    @PutMapping("/{codDisciplina}")
    public  ResponseEntity<Disciplina> alterDisciplina(@Valid @PathVariable Long codDisciplina, @RequestBody Disciplina disciplina){
        return  ResponseEntity.ok(disciplinaService.alter(codDisciplina, disciplina));
    }

    @PutMapping("/matriculados/{idDisciplina}")
    public List<Estudante> matricularEstudante(@PathVariable Long idDisciplina){

        Disciplina disciplina = disciplinaService.getDisciplinaById(idDisciplina);
        return disciplina.getEstudantesMatriculados();

    }

}
