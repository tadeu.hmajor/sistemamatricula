package com.Matriculas.exercicio.Integrador.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;


@Entity
public class Disciplina {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long codDisciplina;

    @NotBlank(message = "Nome obrigatorio")
    private String name;

    @NotBlank(message = "Codigo obrigatorio")
    private String horarioDisciplina;


    private Long turmaDisciplina;

    @ManyToMany(mappedBy = "disciplinasMatriculado")


    private List<Estudante> estudantesMatriculados;

    public Disciplina(String name, String horarioDisciplina, Long turmaDisciplina, List<Estudante> estudantesMatriculados) {
        this.name = name;
        this.horarioDisciplina = horarioDisciplina;
        this.turmaDisciplina = turmaDisciplina;
        this.estudantesMatriculados = estudantesMatriculados;
    }

    public Disciplina() {
    }

    public Long getCodDisciplina() {
        return codDisciplina;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHorarioDisciplina() {
        return horarioDisciplina;
    }

    public void setHorarioDisciplina(String horarioDisciplina) {
        this.horarioDisciplina = horarioDisciplina;
    }

    public Long getTurmaDisciplina() {
        return turmaDisciplina;
    }

    public void setTurmaDisciplina(Long turmaDisciplina) {
        this.turmaDisciplina = turmaDisciplina;
    }

    //evita looping infinito
    @JsonBackReference
    public List<Estudante> getEstudantesMatriculados() {
        return estudantesMatriculados;
    }

    public void setEstudantesMatriculados(List<Estudante> estudantesMatriculados) {
        this.estudantesMatriculados = estudantesMatriculados;
    }

    public void addEstudanteMatriculado(Estudante estudante) {
        estudantesMatriculados.add(estudante);

    }


}
