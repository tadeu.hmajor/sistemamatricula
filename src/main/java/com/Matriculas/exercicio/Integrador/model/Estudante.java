package com.Matriculas.exercicio.Integrador.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;


@Entity
public class Estudante {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idEstudante;

    @NotBlank
    private String nome;

    private int numDocumento;
    @NotBlank
    private String endereco;

    @ManyToMany
    @JoinTable(
            name = "MatriculaAlunoCurso",
            joinColumns = @JoinColumn(name = "estudante_id"),
            inverseJoinColumns = @JoinColumn(name = "disciplina_id")
    )
    private List<Disciplina> disciplinasMatriculado;

    public Estudante(String nome, int numDocumento, String endereco, List<Disciplina> disciplinasMatriculado) {
        this.nome = nome;
        this.numDocumento = numDocumento;
        this.endereco = endereco;
        this.disciplinasMatriculado = disciplinasMatriculado;
    }

    public Estudante() {
    }

    public Long getId(){
        return idEstudante;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getNumDocumento() {
        return numDocumento;
    }

    public void setNumDocumento(int numDocumento) {
        this.numDocumento = numDocumento;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public List<Disciplina> getDisciplinasMatriculado() {
        return disciplinasMatriculado;
    }

    public void setDisciplinasMatriculado(List<Disciplina> disciplinasMatriculado) {
        if(this.disciplinasMatriculado.isEmpty()){
            this.disciplinasMatriculado = new ArrayList<>(disciplinasMatriculado);
        } else{
            this.disciplinasMatriculado.addAll(disciplinasMatriculado);
        }
    }



}
