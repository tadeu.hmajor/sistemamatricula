package com.Matriculas.exercicio.Integrador.service;

import com.Matriculas.exercicio.Integrador.model.Disciplina;
import com.Matriculas.exercicio.Integrador.model.Estudante;
import com.Matriculas.exercicio.Integrador.repository.EstudanteRepository;
import org.hibernate.stat.internal.EntityStatisticsImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class EstudanteService {

    @Autowired
    private EstudanteRepository estudanteRepository;

    @Autowired
    private DisciplinaService disciplinaService;

    public Estudante add(Estudante estudante) {

        boolean doc = verificaDoc(estudante.getNumDocumento());
        if(doc == false) {
            throw new EmptyResultDataAccessException(1);
        } else
           return estudanteRepository.save(estudante);
    }

    public void delete(Long id) {
        estudanteRepository.deleteById(id);
    }

    public List<Estudante> listEstudante() {
        return estudanteRepository.findAll();
    }

    public  List<Estudante> listEstudanteNome(String name){
        List<Estudante> listaNomesprocurado = new ArrayList<Estudante>();
        listaNomesprocurado.clear();
        for(int i=0; i<listEstudante().size(); i++){
            if(listEstudante().get(i).getNome().toLowerCase().contains(name.toLowerCase())) {
                listaNomesprocurado.add(listEstudante().get(i));
            }
        }    return  listaNomesprocurado;
    }
    public Estudante alter(Long id, Estudante estudante) {
        Estudante estudanteAlter = validateEstudante(id);
        BeanUtils.copyProperties(estudante, estudanteAlter, "id");
        return estudanteRepository.save(estudanteAlter);
    }

    public Estudante validateEstudante(Long id) {
        Optional<Estudante> estudante = estudanteRepository.findById(id);
        if (estudante.isEmpty()) {
            throw new EmptyResultDataAccessException(1);
        }
        return estudante.get();
    }

    public boolean verificaDoc(int numDoc) {

        for (int i = 0; i < listEstudante().size(); i++) {
            if (numDoc == listEstudante().get(i).getNumDocumento()) {
                return false;
            }
        } return  true;
    }

    public Estudante getEstudanteById(long codEstudante){

          return estudanteRepository.findById(codEstudante).get();

    }

    public Estudante addDisciplinas( long idEstudante, long idDisciplina){

        Estudante estudante = getEstudanteById(idEstudante);
        Disciplina disciplina = disciplinaService.getDisciplinaById(idDisciplina);
        List <Disciplina> listaDisciplinas = new ArrayList();
        listaDisciplinas.add(disciplina);

        estudante.setDisciplinasMatriculado(listaDisciplinas);
        return estudanteRepository.save(estudante);

    }

}