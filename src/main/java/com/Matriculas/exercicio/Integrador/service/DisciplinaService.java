package com.Matriculas.exercicio.Integrador.service;

import com.Matriculas.exercicio.Integrador.model.Disciplina;
import com.Matriculas.exercicio.Integrador.model.Estudante;
import com.Matriculas.exercicio.Integrador.repository.DisciplinaRepositoy;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DisciplinaService {

    @Autowired
    private DisciplinaRepositoy disciplinaRepositoy;

//    @Autowired
//    private EstudanteService estudanteService;

    public Disciplina add(Disciplina disciplina) {
     return disciplinaRepositoy.save(disciplina);
    }

    public  void delete(Long id){
        disciplinaRepositoy.deleteById(id);
    }

    public List<Disciplina> listDisciplina() {
        return disciplinaRepositoy.findAll();
    }

   public Disciplina alter(long id, Disciplina disciplina){
        Disciplina disciplinaAlter = validadeDisciplina(id);
       BeanUtils.copyProperties(disciplina, disciplinaAlter);
       return  disciplinaRepositoy.save(disciplinaAlter);
   }

    private Disciplina validadeDisciplina(long id) {

        Optional<Disciplina> disciplina = disciplinaRepositoy.findById(id);
        if(disciplina.isEmpty()) {
            throw new EmptyResultDataAccessException(1);
        }
        return  disciplina.get();
    }

    public Disciplina getDisciplinaById(long id){
        return disciplinaRepositoy.findById(id).get();
    }

    public  List<Estudante> getListAlunosMatriculados(Disciplina disciplina){
        return  disciplina.getEstudantesMatriculados();
    }



}
