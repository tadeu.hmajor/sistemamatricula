package com.Matriculas.exercicio.Integrador.controller;

import com.Matriculas.exercicio.Integrador.model.Disciplina;
import com.Matriculas.exercicio.Integrador.model.Estudante;
import com.Matriculas.exercicio.Integrador.service.DisciplinaService;
import com.Matriculas.exercicio.Integrador.service.EstudanteService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


// usada para testar aplicações Spring MVC (@RestControllers
//estão dentro do Spring MVC).

@WebMvcTest(value = EstudanteController.class)
public class EstudanteControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private EstudanteService estudanteService;

    @MockBean
    private DisciplinaService disciplinaService;

    @Test
    public void deveRetornarSucesso_QuandoCadastrarEstudante() throws Exception {

        String conteudoJson = "{\"nome\":\"joao\",\"numDocumento\": 25312, \"endereco\":\"Av Afonso Pena 100\"}";
        Estudante mockEstudante = new Estudante("joao", 25312, "Av Afonso Pena 100",null);


        Mockito.when(
                estudanteService.add(Mockito.any(Estudante.class))).thenReturn(mockEstudante);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/estudantes")
                .accept(MediaType.APPLICATION_JSON)
                .content(conteudoJson).contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        String esperado = "{\"id\":null,\"nome\":\"joao\",\"numDocumento\": 25312,\"endereco\":\"Av Afonso Pena 100\",\"disciplinasMatriculado\":null}";

        JSONAssert.assertEquals(esperado,result.getResponse().getContentAsString(), true);

    }
}
